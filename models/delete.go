package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/discord-social-media-manager/utils"
)

// DeleteDiscord struct
type DeleteDiscord struct {
	GuildID    string   `json:"guild_id"`
	MessageIDs []string `json:"message_ids"`
}

// DeleteRequest struct
type DeleteRequest struct {
	Discord *DeleteDiscord `json:"discord"`
}

// Twitter send the request to the Twitter microservice
func (r *DeleteRequest) Twitter(c *utils.Config, authToken string) *utils.ModelError {
	httpClient := &http.Client{
		Timeout: time.Second * 60,
	}

	requestBody, jsonErr := json.Marshal(r)

	if jsonErr != nil {
		modelErr := utils.NewModelError(jsonErr)
		modelErr.SetMessage("Could not marshal DeleteRequest struct")
		modelErr.SetStatus(http.StatusInternalServerError)
		return modelErr
	}

	url := c.Twitter.URL + "/twitter"

	newRequest, newRequestErr := http.NewRequest(http.MethodDelete, url, bytes.NewBuffer(requestBody))

	if newRequestErr != nil {
		modelErr := utils.NewModelError(newRequestErr)
		modelErr.SetMessage("Failed to create new request to delete Twitter post")
		modelErr.SetStatus(http.StatusInternalServerError)
		return modelErr
	}

	newRequest.Header.Add("Auth-Token", authToken)
	newRequest.Header.Add("Content-Type", "application/json")

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		modelErr := utils.NewModelError(requestErr)
		modelErr.SetMessage("Failed to send request to delete Twitter post")
		modelErr.SetStatus(http.StatusBadRequest)
		return modelErr
	}

	switch response.StatusCode {
	case http.StatusOK:
		return nil
	case http.StatusBadRequest:
		modelErr := utils.NewModelError(fmt.Errorf("Failed to delete tweet"))
		modelErr.SetMessage("Bad request response when sending delete tweet request")
		modelErr.SetStatus(http.StatusBadRequest)
		return modelErr
	default:
		modelErr := utils.NewModelError(fmt.Errorf("Failure response from sending delete tweet with status code (%d)", response.StatusCode))
		modelErr.SetMessage("Bad response from deleting Twitter post")
		modelErr.SetStatus(response.StatusCode)
		return modelErr
	}
}

// Facebook sends the request to the Facebook microservice
func (r *DeleteRequest) Facebook(c *utils.Config) {

}
