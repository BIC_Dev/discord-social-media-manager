package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/discord-social-media-manager/utils"
)

// CreateDiscord struct
type CreateDiscord struct {
	GuildID         string `json:"guild_id"`
	GuildName       string `json:"guild_name"`
	ChannelID       string `json:"channel_id"`
	ChannelName     string `json:"channel_name"`
	MessageID       string `json:"message_id"`
	MessageAuthorID string `json:"message_author_id"`
	MessageAuthor   string `json:"message_author"`
}

// CreateRequest struct
type CreateRequest struct {
	Discord     *CreateDiscord `json:"discord"`
	MessageText string         `json:"message_text"`
	MediaURLs   []string       `json:"media_urls"`
}

// CreateResponse struct
type CreateResponse struct {
	Status        int                    `json:"status"`
	Message       string                 `json:"message"`
	TwitterIDs    []string               `json:"twitter_ids"`
	TwitterUrls   []string               `json:"twitter_urls"`
	TwitterErrors []*CreateEmbeddedError `json:"twitter_errors"`
	DBErrors      []*CreateEmbeddedError `json:"db_errors"`
}

// CreateEmbeddedError struct
type CreateEmbeddedError struct {
	DiscordMessageID string `json:"discord_message_id"`
	Message          string `json:"message"`
	Error            string `json:"error"`
}

// Twitter send the request to the Twitter microservice
func (r *CreateRequest) Twitter(c *utils.Config, authToken string) (*CreateResponse, *utils.ModelError) {
	httpClient := &http.Client{
		Timeout: time.Second * 60,
	}

	requestBody, jsonErr := json.Marshal(r)

	if jsonErr != nil {
		modelErr := utils.NewModelError(jsonErr)
		modelErr.SetMessage("Could not marshal CreateRequest struct")
		modelErr.SetStatus(http.StatusInternalServerError)
		return nil, modelErr
	}

	url := c.Twitter.URL + "/twitter"

	newRequest, newRequestErr := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBody))

	if newRequestErr != nil {
		modelErr := utils.NewModelError(newRequestErr)
		modelErr.SetMessage("Failed to create new request to create Twitter post")
		modelErr.SetStatus(http.StatusInternalServerError)
		return nil, modelErr
	}

	newRequest.Header.Add("Auth-Token", authToken)
	newRequest.Header.Add("Content-Type", "application/json")

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		modelErr := utils.NewModelError(requestErr)
		modelErr.SetMessage("Failed to send request to create Twitter post")
		modelErr.SetStatus(http.StatusBadRequest)
		return nil, modelErr
	}

	switch response.StatusCode {
	case http.StatusCreated:
		var responseStruct *CreateResponse
		jsonErr := json.NewDecoder(response.Body).Decode(&responseStruct)

		if jsonErr != nil {
			modelErr := utils.NewModelError(fmt.Errorf("Bad response data from create Twitter post"))
			modelErr.SetMessage("Successful response from Twitter microservice with bad data in response")
			modelErr.SetStatus(response.StatusCode)
			return nil, modelErr
		}

		return responseStruct, nil
	default:
		modelErr := utils.NewModelError(fmt.Errorf("Failure response from sending create tweet with status code (%d)", response.StatusCode))
		modelErr.SetMessage("Bad response from create Twitter post")
		modelErr.SetStatus(response.StatusCode)
		return nil, modelErr
	}
}

// Facebook sends the request to the Facebook microservice
func (r *CreateRequest) Facebook(c *utils.Config) {

}
