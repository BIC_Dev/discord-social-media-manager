package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/discord-social-media-manager/models"
	"gitlab.com/BIC_Dev/discord-social-media-manager/utils"
)

// Post struct
type Post struct {
	DiscordGuild     *discordgo.Guild
	DiscordChannel   *discordgo.Channel
	DiscordMessageID string
	MessageAuthorID  string
	MessageAuthor    string
	Description      string
	MediaURLs        []string
}

// Config is the config struct
var Config *utils.Config

// Log is the logging struct
var Log *utils.Log

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	Log = utils.InitLog(logLevel)

	discordToken := os.Getenv("DISCORD_TOKEN")

	Config = utils.GetConfig(env)

	dg, discErr := discordgo.New("Bot " + discordToken)

	if discErr != nil {
		Log.Log(fmt.Sprintf("ERROR: Error creating Discord session: %s", discErr.Error()), Log.LogFatal)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// Register the messageDelete func as a callback for MessageDelete events
	dg.AddHandler(messageDelete)

	// Register the messageDeleteBulk func as a callback for MessageDeleteBulk
	dg.AddHandler(messageDeleteBulk)

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		Log.Log(fmt.Sprintf("ERROR: Error opening connection: %s", openErr.Error()), Log.LogFatal)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	Log.Log("PROCESS: Bot is now running", Log.LogInformation)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	Log.Log(fmt.Sprintf("PROCESS: MessageCreate event for Message ID: %s", m.ID), Log.LogInformation)

	if m.Author.ID == s.State.User.ID {
		Log.Log("BLOCKED: MessageCreate event sent by Social Media Messaging Bot", Log.LogInformation)
		return
	}

	if !isDiscordWhitelisted(m.GuildID) {
		Log.Log(fmt.Sprintf("BLOCKED: MessageCreate event sent from non-whitelisted guild: %s", m.ChannelID), Log.LogInformation)
		return
	}

	if len(m.Attachments) <= 0 {
		Log.Log("BLOCKED: MessageCreate event sent without attachement", Log.LogInformation)
		return
	}

	var imageUrls []string
	maxImageSizeBytes := 5000000

	for _, v := range m.Attachments {
		if v.Size > maxImageSizeBytes {
			Log.Log("BLOCKED: MessageCreate event sent with image > max size allowed", Log.LogInformation)
			continue
		}

		if !validImageType(v.Filename) {
			Log.Log("BLOCKED: MessageCreate event sent with invalid image type", Log.LogInformation)
			continue
		}

		imageUrls = append(imageUrls, v.URL)
	}

	if len(imageUrls) <= 0 {
		Log.Log("BLOCKED: MessageCreate event sent without valid attachment", Log.LogInformation)
		return
	}

	guild, guildErr := s.Guild(m.GuildID)

	if guildErr != nil {
		Log.Log(fmt.Sprintf("ERROR: Error getting guild for ID (%s): %s", m.GuildID, guildErr.Error()), Log.LogHigh)
	}

	channel, channelErr := s.Channel(m.ChannelID)

	if channelErr != nil {
		Log.Log(fmt.Sprintf("ERROR: Error getting channel for ID (%s): %s", m.ChannelID, channelErr.Error()), Log.LogHigh)
	}

	createRequest := models.CreateRequest{
		Discord: &models.CreateDiscord{
			GuildID:         guild.ID,
			GuildName:       guild.Name,
			ChannelID:       channel.ID,
			ChannelName:     channel.Name,
			MessageID:       m.ID,
			MessageAuthorID: m.Author.ID,
			MessageAuthor:   m.Author.Username,
		},
		MessageText: m.Content,
		MediaURLs:   imageUrls,
	}

	Log.Log("PROCESS: Sending create request to Twitter microservice", Log.LogInformation)

	tweetResponse, tweetErr := createRequest.Twitter(Config, os.Getenv("TWITTER_SERVICE_AUTH_TOKEN"))

	if tweetErr != nil {
		Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogHigh)
		return
	}

	Log.Log("SUCCESS: Got success response for create from Twitter microservice", Log.LogInformation)
	reactionErr := addReaction(s, m, guild, "TWITTER")

	if reactionErr != nil {
		Log.Log(fmt.Sprintf("ERROR: Failed to add Twitter reaction: %s", reactionErr.Error()), Log.LogMedium)
	}

	Log.Log("PROCESS: Sending direct message with Twitter URLs", Log.LogInformation)

	directMessageErr := sendDirectMessage(s, channel.ID, m.Author.ID, tweetResponse)

	if directMessageErr != nil {
		Log.Log(fmt.Sprintf("ERROR: Failed to send direct message with Twitter URLs: %s", directMessageErr.Error()), Log.LogMedium)
	} else {
		Log.Log("SUCCESS: Sent direct message with Twitter URLs", Log.LogInformation)
	}
}

func messageDelete(s *discordgo.Session, m *discordgo.MessageDelete) {
	Log.Log(fmt.Sprintf("PROCESS: MessageDelete event for Message ID: %s", m.ID), Log.LogInformation)

	if !isDiscordWhitelisted(m.GuildID) {
		Log.Log(fmt.Sprintf("BLOCKED: MessageDelete event sent from non-whitelisted guild: %s", m.ChannelID), Log.LogInformation)
		return
	}

	var discordDeletes *models.DeleteDiscord
	var discordMessages []string

	discordMessages = append(discordMessages, m.ID)

	discordDeletes = &models.DeleteDiscord{
		GuildID:    m.GuildID,
		MessageIDs: discordMessages,
	}

	deleteRequest := models.DeleteRequest{
		Discord: discordDeletes,
	}

	Log.Log("PROCESS: Sending delete request to Twitter microservice", Log.LogInformation)
	tweetErr := deleteRequest.Twitter(Config, os.Getenv("TWITTER_SERVICE_AUTH_TOKEN"))

	if tweetErr != nil {
		switch tweetErr.GetStatus() {
		case http.StatusBadRequest:
			Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogLow)
		case http.StatusInternalServerError:
			Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogHigh)
		default:
			Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogHigh)
		}

	} else {
		Log.Log("SUCCESS: Got success response for delete from Twitter microservice", Log.LogInformation)
	}
}

func messageDeleteBulk(s *discordgo.Session, m *discordgo.MessageDeleteBulk) {
	Log.Log(fmt.Sprintf("PROCESS: MessageDeleteBulk event for Message IDs: %s", fmt.Sprintf("%v", m.Messages)), Log.LogInformation)

	if !isDiscordWhitelisted(m.GuildID) {
		Log.Log(fmt.Sprintf("BLOCKED: MessageDeleteBulk event sent from non-whitelisted guild: %s", m.ChannelID), Log.LogInformation)
		return
	}

	var discordDeletes *models.DeleteDiscord
	var discordMessages []string

	discordDeletes = &models.DeleteDiscord{
		GuildID: m.GuildID,
	}

	for _, messageID := range m.Messages {
		discordMessages = append(discordMessages, messageID)
	}

	discordDeletes.MessageIDs = discordMessages

	deleteRequest := models.DeleteRequest{
		Discord: discordDeletes,
	}

	Log.Log("PROCESS: Sending bulk delete request to Twitter microservice", Log.LogInformation)
	tweetErr := deleteRequest.Twitter(Config, os.Getenv("TWITTER_SERVICE_AUTH_TOKEN"))

	if tweetErr != nil {
		switch tweetErr.GetStatus() {
		case http.StatusBadRequest:
			Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogLow)
		case http.StatusInternalServerError:
			Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogHigh)
		default:
			Log.Log(fmt.Sprintf("ERROR: %s: %s", tweetErr.GetMessage(), tweetErr.Error()), Log.LogHigh)
		}

	} else {
		Log.Log("SUCCESS: Got success response for bulk delete from Twitter microservice", Log.LogInformation)
	}

}

func isDiscordWhitelisted(guildID string) bool {
	for _, id := range Config.DiscordWhitelist {
		if guildID == id {
			return true
		}
	}

	return false
}

// validImageType checks if the image sent in discord is a valid format for Twitter
func validImageType(imageName string) bool {
	validTypes := []string{
		".jpg",
		".jpeg",
		".png",
	}

	for _, v := range validTypes {
		if strings.Contains(imageName, v) {
			return true
		}
	}

	return false
}

func addReaction(s *discordgo.Session, m *discordgo.MessageCreate, g *discordgo.Guild, service string) error {
	Log.Log(fmt.Sprintf("PROCESS: Adding %s reaction for message ID: %s", service, m.Message.ID), Log.LogInformation)
	twitterEmojiID := Config.Twitter.EmojiDefault

	for _, emoji := range g.Emojis {
		if emoji.Name == Config.Twitter.EmojiCustom {
			twitterEmojiID = emoji.APIName()
		}
	}

	reactionError := s.MessageReactionAdd(m.ChannelID, m.Message.ID, twitterEmojiID)

	if reactionError != nil {
		return reactionError
	}

	Log.Log(fmt.Sprintf("SUCCESS: Added %s reaction for message ID: %s", service, m.Message.ID), Log.LogInformation)

	return nil
}

func sendDirectMessage(s *discordgo.Session, channelID string, authorID string, tweetResponse *models.CreateResponse) error {
	userChannel, userChannelErr := s.UserChannelCreate(authorID)

	if userChannelErr != nil {
		return userChannelErr
	}

	directMessageText := generateDirectMessageText(channelID, tweetResponse.TwitterUrls)

	_, messageErr := s.ChannelMessageSend(userChannel.ID, directMessageText)

	if messageErr != nil {
		return messageErr
	}

	return nil
}

func generateDirectMessageText(channelID string, urls []string) string {
	var message string

	if len(urls) > 1 {
		message = fmt.Sprintf("The images you posted in <#%s> have been uploaded to Twitter!", channelID)
	} else {
		message = fmt.Sprintf("The image you posted in <#%s> has been uploaded to Twitter!", channelID)
	}

	for _, url := range urls {
		message = message + fmt.Sprintf("\n%s", url)
	}

	return message
}
