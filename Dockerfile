FROM golang:1.12-alpine AS build_base

RUN apk add --no-cache git
RUN apk add build-base

# Set the Current Working Directory inside the container
WORKDIR /tmp/discord-social-media-mangager

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Build the Go app
RUN go build -o ./manager .

FROM alpine:3.9 
RUN apk add ca-certificates
RUN apk add jq

COPY --from=build_base /tmp/discord-social-media-mangager/manager /manager
COPY --from=build_base /tmp/discord-social-media-mangager/configs/ /configs/
COPY --from=build_base /tmp/discord-social-media-mangager/scripts/startup.sh /scripts/startup.sh

# Run the startup script
CMD ["sh", "/scripts/startup.sh"]