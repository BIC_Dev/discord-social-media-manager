export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export DISCORD_TOKEN=$(echo ${ENV_VARS} | jq -r '.DISCORD_TOKEN')
export TWITTER_SERVICE_AUTH_TOKEN=$(echo ${ENV_VARS} | jq -r '.TWITTER_SERVICE_AUTH_TOKEN')
export LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.LOG_LEVEL')

/manager