module gitlab.com/BIC_Dev/discord-social-media-manager

go 1.14

require (
	github.com/bwmarrin/discordgo v0.20.3
	gopkg.in/yaml.v2 v2.3.0
)
