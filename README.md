# Discord Social Media Image Manager

A Discord bot that posts images from Discord to social media services. This bot binds to message create, delete, and bulk delete events. It then sends valid messages with image attachments to social media microservices, which then post to the social media services.

## Social Media Services
1. Twitter
2. Facebook (in development)
3. Instagram (upcomming development work)

# How to Use

## Setup
1. Create new Role for this bot
2. Set permissions to only allow this role on specific channels
3. Add the bot to Discord
4. Immediately attach the created role for it

## Running

When the bot is running, any valid image posted to a channel where the bot is assigned will be posted to the available social media services. Once posted to a social media service, a reaction will be added to the Discord post using the symbol for that social media service. If you delete the post, the asset will also be removed from all social media services it was posted to.

# How To Add to Your Discord Guild

## Option 1: Utilize my cluster (FREE for now)
1. Reach out to me on Discord: Bic#7317
2. We can discuss deploying this for your Discord guild

## Option 2: Deploy your own cluster ($10-30 per month)
1. Fork repo
1. Set up AWS RDS PostgreSQL instance
1. Set up AWS ECS Cluster
1. Set up AWS ECR
1. Set up AWS ALB
1. Set up AWS Security Groups
1. Set up AWS Secrets Manager keys
1. Set up AWS ECS Service
1. Update /aws/discord-social-media-manager-task-definition.json to use your account
1. Deploy to AWS using Gitlab pipeline